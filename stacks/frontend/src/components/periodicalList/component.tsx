import * as React from 'react';
import { Link } from 'react-router-dom';

import { InitialisedPeriodical } from '../../../../../lib/interfaces/Periodical';

interface Props {
  periodicals: InitialisedPeriodical[];
}

export const PeriodicalList = (props: Props) => (
  props.periodicals.length === 0
    ? null
    : (
        <ul>
          {props.periodicals.map(renderPeriodical)}
        </ul>
      )
);

function renderPeriodical(periodical: InitialisedPeriodical) {
  return (
    <li
      itemScope={true}
      itemType="https://schema.org/Periodical"
      className="media"
      key={periodical.identifier}
    >
      <div className="media-content">
        <div className="content">
          <p title={periodical.description}>
            <Link itemProp="url" to={`/journal/${periodical.identifier}`}>
              <strong>{periodical.name ? periodical.name : 'Unnamed journal'}</strong>
            </Link>
            {renderHeadline(periodical.headline)}
          </p>
        </div>
      </div>
    </li>
  );
}

function renderHeadline(headline?: string) {
  if (!headline) {
    return null;
  }

  return (
    <>
      <br/>{headline}
    </>
  );
}
